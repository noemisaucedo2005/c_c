#include <iostream>
#include <cmath>
using namespace std;

bool isprime(long long int p){
	bool prime = true;
	long long int mff,divisor;
	mff = (long long int)sqrt(p)+1;
	divisor = 3;
	while (divisor < mff && prime == true){
		if(p % divisor ==0){
			prime = false;
		}
		divisor++;
	}
	return prime;
}
int main(){
long long int n;
int count =1;
n=3;
bool prime;
cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
	while (n< 101){
		//cout <<n<<endl;
		prime =isprime(n);
		if (prime){
			count++;
			cout<<n<<"\t"<<count<<"\t"<<prime<<endl;
		}
		n = n +2;
	}
}
